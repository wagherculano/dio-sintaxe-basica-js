// if, else if, else
var jogador1 = 1;
var jogador2 = 0;

if (jogador1 > 0) {
    console.log('jogador1 marcou ponto')
} else if (jogador2 > 0) {
    console.log('jogador2 marcou ponto')
} else {
    console.log('ninguém marcou ponto')
}

//switch case
switch(2 > 1){
    case true:
        console.log('2 é maior que 1')
        break
    case false:
        console.log('2 não é maior que 1')
        break
    default:
        console.log('Um valor default qualquer!')
}

//for
var array = ['a', 'b', 'c', 'd']
for(let i = 0; i < array.length; i++){
    console.log(i, array[i])
}

//for in
var array = ['a', 'b', 'c', 'd']
for(index in array){
    console.log(index, array[index])
}

var obj = {nome: 'Wagner', idade: 33}
for(index in obj){
    console.log(obj[index])
}

//for of
var array = ['a', 'b', 'c', 'd']
for(item of array){
    console.log(item)
}

//while
var a = 0;
while (a < 10) {
    a++;
    console.log('Valor de a:', a);
}

//Do While
var a = 0;
do {
    a++;
    console.log(a);
} while (a < 10)