// Tipos primitivos

//Boolean
var VouF = true;
console.log(VouF);
console.log(typeof(VouF));

//Number
var num = 10
console.log(num)
console.log(typeof(num))

//String
var str = 'Wagner'
console.log(str)
console.log(typeof(str))


// Variáveis
var variavel = "variavel"
variavel = "nova variavel"
console.log(variavel)

let variavel_2 = "var2"
variavel_2 = "var_2"
console.log(variavel_2)

const variavel_3 = "variavel_constante"
console.log(variavel_3)

function escopoLocal(){
    let varLocal = "Variavel Local da Funcao"
    console.log(varLocal) // a leitura dessa variavel so acontecerá dentro dessa funcao
}
escopoLocal()