var array = ['a', 'b', 'c']
console.log('Testando forEach')
array.forEach(function(item, index){console.log(index, item)})

var array = ['a', 'b', 'c']
console.log("Testando push(d)")
array.push('d')
console.log(array)

var array = ['a', 'b', 'c']
console.log('Testando pop')
array.pop()
console.log(array)

var array = ['a', 'b', 'c']
array.shift()
console.log('Testando shift')
console.log(array)

var array = ['a', 'b', 'c']
console.log('Testando unshift')
array.unshift(42)
console.log(array)

var array = ['a', 'b', 'c']
console.log('Testando indexOf')
console.log(array.indexOf('b'))


var array = ['a', 'b', 'c']
console.log('Testando splice')
var new_array = array.splice(1, 2, 'B', 'C')
console.log(array)
// Se não tivesse passado 'B', 'C' como parametro, array teria apenas ['a'] como valor.
console.log(new_array)

var array = ['a', 'b', 'c']
console.log('Testando slice')
console.log(array.slice(0,2))
