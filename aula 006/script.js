//Funcao declarativa
function FuncaoDeclarativa(){
    console.log('Ola FuncaoDeclarativa')
}
FuncaoDeclarativa()

//Expressao de Função
var func_1 = function funcNomeada(){
    console.log('Chamando expressao de funcao nomeada')
}
func_1()

var func_2 = function(){
    console.log('Chamando expressao de funcao anonima/sem nome')
}
func_2()

//Arrow Function
var ola_seta = () => {
    console.log('Olá Arrow Function')
}
ola_seta()