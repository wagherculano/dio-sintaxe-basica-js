function calc(){
    const operacao = Number(prompt('1 Soma +\n2 Subtracao -\n3 Divisao /\n4 Multiplicacao *\n5 Modulo %\n6 Potenciacao **\n'))

    if(!operacao || operacao >= 7){
        alert('Opcao Inválida!')
        calc()
    }else{
        let n1 = Number(prompt('Digite o 1º valor:\n'))
        let n2 = Number(prompt('Digite o 2º valor:\n'))
        let result

        if(!n1 || !n2){
            alert('Valores inválidos!')
            calc()
        }else{
            function soma(a, b){
                result = a + b
                alert(`${a} + ${b} = ${result}`)
            }

            function subtracao(a, b){
                result = a - b
                alert(`${a} - ${b} = ${result}`)
            }

            function div(a, b){
                result = a / b
                alert(`${a} / ${b} = ${result}`)
            }

            function mult(a, b){
                result = a * b
                alert(`${a} * ${b} = ${result}`)
            }

            function mod(a, b){
                result = a % b
                alert(`${a} % ${b} = ${result}`)
            }

            function pow(a, b){
                result = a ** b
                alert(`${a} ** ${b} = ${result}`)
            }

            function novaOperacao(){
                const opc = Number(prompt('Deseja continuar?\n1 - SIM\n2 - NÃO\n'))

                if(opc == 1){
                    calc()
                }else if(opc == 2){
                    alert('Saindo...\n')
                }else{
                    alert('Opcao Inválida!\n')
                    novaOperacao()
                }
            }
        }
        switch(operacao){
            case 1:
                soma(n1, n2)
                novaOperacao()
                break;
            case 2:
                subtracao(n1, n2)
                novaOperacao()
                break;
            case 3:
                div(n1, n2)
                novaOperacao()
                break;
            case 4:
                mult(n1, n2)
                novaOperacao()
                break;
            case 5:
                mod(n1, n2)
                novaOperacao()
                break;
            case 6:
                pow(n1, n2)
                novaOperacao()
                break;
            default:
                alert(`Valor ${operacao} Inválido!`)
                novaOperacao()
        }
    }
}
calc();