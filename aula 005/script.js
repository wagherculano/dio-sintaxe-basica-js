function nomeDaFuncao() {
    console.log('Hello World!')
}
    
nomeDaFuncao();

function nomeDaFuncaoComParametro(param1, param2) {
    console.log(param1, param2)
}
    
nomeDaFuncaoComParametro("Hello", "World!")